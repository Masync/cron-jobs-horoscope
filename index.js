const cron = require('node-cron')
const {db,http,signs} = require('./utils')


class Main
{
    static async getHoroscope(){
        const allHoroscope= []
        for (const sign of signs) {
            const { horoscope } = (await http.get('/' + sign)).data
            allHoroscope.push({
                sign,
                horoscope
            })
            
        }
        const name = new Date().toDateString().split(" ").join("_")
        db.setItem(name,JSON.stringify(allHoroscope))
    }

}

cron.schedule("* * * * *", ()=>{
    Main.getHoroscope();
})